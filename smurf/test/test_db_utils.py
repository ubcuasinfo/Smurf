import testing.postgresql
import psycopg2
from datetime import datetime

from unittest import TestCase
from smurf.db_utils import config, db_write


class TestDBUtils(TestCase):
    def setUp(self):
        self.postgres = testing.postgresql.Postgresql()
        conn = psycopg2.connect(**self.postgres.dsn())
        cursor = conn.cursor()
        cursor.execute('CREATE TABLE interop_uastelemetry(id int, \
                                                                  latitude float(8), \
                                                                  longitude float(8), \
                                                                  altitude_msl float(4), \
                                                                  uas_heading float(4),\
                                                                  created_at TIMESTAMP,\
                                                                  uploaded bool)')
        cursor.close()
        conn.commit()
        conn.close()

    def test_config_valid(self):
        self.assertDictEqual(config('../database.ini'), {
            'host': 'localhost',
            'database': 'postgres',
            'user': 'postgres'
        })

    def test_config_invalid_file(self):
        with self.assertRaises(IOError):
            config()

    def test_config_invalid_section(self):
        with self.assertRaises(IOError):
            config(filename='../database.ini', section='mysql')

    def test_db_write(self):
        cols = ('latitude',
                'longitude',
                'altitude_msl',
                'uas_heading',
                'created_at',
                'uploaded')

        data = (
            '49.12345678',
            '-123.9124125',
            '59.124',
            '123',
            datetime.now(),
            False
        )

        self.assertTrue(db_write(app='interop', model='uastelemetry',
                                 config_path="../database.ini", cols=cols, data=data))

    def test_db_write_invalid_table(self):
        cols = ('latitude',
                'longitude',
                'altitude_msl',
                'uas_heading',
                'created_at',
                'uploaded')

        data = (
            '49.12345678',
            '-123.9124125',
            '59.124',
            '123',
            datetime.now(),
            False
        )

        self.assertFalse(db_write(app='interop', model='fake',
                                  config_path="../database.ini", cols=cols, data=data))

    def test_db_write_value_mismatch(self):
        cols = ('latitude',
                'longitude',
                'altitude_msl',
                'uas_heading',
                'created_at')

        data = (
            '49.12345678',
            '-123.9124125',
            '59.124',
            '123',
            datetime.now(),
        )

        self.assertFalse(db_write(app='interop', model='uastelemetry',
                                  config_path="../database.ini", cols=cols, data=data))

    def test_db_write_invalid_value(self):
        cols = ('latitude',
                'longitude',
                'altitude_msl',
                'uas_heading',
                'created_at',
                'uploaded')

        data = (
            'wrong',
            '-123.9124125',
            '59.124',
            '123',
            datetime.now(),
            False
        )

        self.assertFalse(db_write(app='interop', model='uastelemetry',
                                  config_path="../database.ini", cols=cols, data=data))

    def tearDown(self):
        self.postgres.stop()
