import psycopg2
import traceback

from configparser import ConfigParser


def config(filename='database.ini', section='postgresql'):
	"""Sets up database configuration based on config file
	
	Keyword Arguments:
		filename {str} -- path to config file (default: {'smurf/database.ini'})
		section {str} -- database type (default: {'postgresql'})
	
	Returns:
		dict -- connected psycopg2 database object
	
	Raises:
		IOError -- If there is an error parsing the config file
	"""

	# create a parser
	parser = ConfigParser()
	# read config file
	parser.read(filename)

	# get section, default to postgresql
	if not parser.has_section(section):
		raise IOError('Section {0} not found in the {1} file'.format(section, filename))

	db = {}
	params = parser.items(section)
	for param in params:
		db[param[0]] = param[1]
		
	return db


def db_write(app="interop", model="uastelemetry", config_path="database.ini", cols=(), data=()):
	"""Writes data to a table in a database
	
	Keyword Arguments:
		app {str} -- Name of Django app (default: {"interop"})
		model {str} -- Name of Django model (default: {"uastelemetry"})
		cols {tuple} -- Columns in table to write to (default: {()})
		data {tuple} -- Values to write, must match the columns
						of the table in number and type (default: {()})
	
	Returns:
		bool -- True if db write operation was successful
	"""
	conn = None

	cmd = "INSERT INTO " + app + "_" + model + " (" + ", ".join(cols) + ") VALUES %s"
	query_args = (data,)

	try:
		params = config(config_path)
		conn = psycopg2.connect(**params)
		cur = conn.cursor()
		cur.execute(cmd, query_args)

		conn.commit()

	except Exception:
		traceback.print_exc()

		if conn is not None:
			conn.close()

		return False

	if conn is not None:
		conn.close()

		return True