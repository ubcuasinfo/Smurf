#!/usr/bin/env python2

"""
This module will take x arguments:
- baud_rate
- port_number

Smurf will update the database once it's connected. It will have the following models:

- latitude
- longitude
- altitude_msl
- uas_heading
- created_at
- uploaded

Database settings should be added to database.ini
"""

import dronekit
import argparse
import serial
import traceback
import time
import signal
import sys
import logging
import os
from db_utils import db_write
from datetime import datetime

logging.basicConfig(level=logging.DEBUG, format='DEBUG - %(message)s')
#logging.disable(logging.DEBUG)

HOME_LOCATION_TIMEOUT = 5
DEBUG = 1
FREQ = 0.5
ATTEMPT_DELAY = 1
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))


def loop_for_connection(connection_string, baud_rate):
	"""
	Keep checking the port provided until connected
	Args:
		connection_string: the port number given
		baud_rate: baud rate of the connection
	Returns:
		None
	"""

	def signal_handler(sig, frame):
		# this function will trap the ctrl C inputs, in case when the user when to terminate the program.
		print('=============You pressed Ctrl+C!')
		if vehicle:
			vehicle.close()
			logging.debug("Vehicle disconnected and closed!")
		else:
			logging.debug("Vehicle not connected!")

		sys.exit(0)

	signal.signal(signal.SIGINT, signal_handler)

	is_connected = False
	vehicle = None

	while True:
		logging.debug("Attempting to connecting to vehicle on: %s at baud: %s\n" %
		(connection_string, baud_rate))

		# This try catch block will attempt to connect to the vehicle.
		try:
			#
			vehicle = dronekit.connect(connection_string,
									   wait_ready=True,
									   baud=baud_rate,
									   heartbeat_timeout=5)

		# try to catch the serial false.

		except serial.SerialException:
			traceback.print_exc()
			time.sleep(ATTEMPT_DELAY)
		except dronekit.APIException:
			traceback.print_exc()
			time.sleep(ATTEMPT_DELAY)
		except IOError:
			traceback.print_exc()
			time.sleep(ATTEMPT_DELAY)
		except Exception:
			traceback.print_exc()
			time.sleep(ATTEMPT_DELAY)
		else:
			# connection is established.
			is_connected = True
			logging.debug("Vehicle connected at %s %s\n" % (connection_string, baud_rate))
	
		if not is_connected and not vehicle:
			continue

		prev_last_heartbeat = -1

		while is_connected and prev_last_heartbeat != vehicle.last_heartbeat:
			"""
			This is the operation loop. At this point the copter should be connected
			We should expect disconnection at this loop
			"""
			try:
				prev_last_heartbeat = vehicle.last_heartbeat

				# if the home location is not received, just busy wait.
				while not vehicle.home_location:
					cmds = vehicle.commands
					cmds.download()
					cmds.wait_ready()
					time.sleep(ATTEMPT_DELAY)
					logging.debug("Waiting for home location\n")

				# We have a home location, so print it!

				logging.debug("lon: %s" % vehicle.location.global_relative_frame.lon)
				logging.debug("lat: %s" % vehicle.location.global_relative_frame.lat)
				logging.debug("alt: %s" % vehicle.location.global_relative_frame.alt)
				logging.debug("hdg: %s\n" % vehicle.heading)

				cols = ('latitude',
						'longitude',
						'altitude_msl',
						'uas_heading',
						'created_at',
						'uploaded')

				data = (
					vehicle.location.global_relative_frame.lat,
					vehicle.location.global_relative_frame.lon,
					vehicle.location.global_relative_frame.alt,
					vehicle.heading,
					datetime.now(),
					False
				)

				db_write(app='interop',
						 model='uastelemetry',
						 config_path=os.path.join(ROOT_DIR, "database.ini"),
						 cols=cols,
						 data=data)

			except Exception:
				traceback.print_exc()
				is_connected = False
				logging.debug("Unexpected connection failed in loop!")

			time.sleep(FREQ)

		is_connected = False

		logging.debug("Connection failed when looping!")
		time.sleep(ATTEMPT_DELAY)


def main():
	"""
	The main execution loop of
	Args:

	Returns:

	"""
	parser = argparse.ArgumentParser(
		description='Retrieves vehicle information and writes to a database.')
	parser.add_argument('--connect',
	                    help="vehicle connection target string.\
						If not specified, will default to /dev/ttyUSB0")
	parser.add_argument('--baud',
						help="vehicle connection baud rate.\
						If not specified, the deafult will be 115200")
	args = parser.parse_args()

	connection_string = args.connect or '/dev/ttyUSB0'
	baud_rate = args.baud or '115200'

	loop_for_connection(connection_string, baud_rate)


if __name__ == '__main__':
	main()

