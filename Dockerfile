# UBC UAS Dockerfile - Smurf

# Pull the Python base image
FROM python:2.7

RUN mkdir -p /uas/smurf
WORKDIR /uas/smurf

RUN pip install pipenv

# Install Python Dependencies
COPY Pipfile* ./
RUN pipenv install

COPY . /uas/smurf

WORKDIR /uas/smurf/smurf
ENTRYPOINT ["pipenv", "run", "python", "smurf.py"]
